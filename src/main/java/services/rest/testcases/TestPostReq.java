package services.rest.testcases;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;


import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import io.restassured.response.Response;

import utilities.InitTests;


import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyContains;
import static utilities.RestAPI.postWithResponse;
import static utilities.RestAPI.get;
import static verify.SoftAssertions.verifyEquals;
public class TestPostReq extends InitTests
{
	
	@Test(priority=1)
	public static void deleteJobs() throws IOException
	{
		ExtentTest test=null;

		try {
			test = reports.createTest("deleteJobs");
			test.assignCategory("REST");
			String path = dir_path+"\\src\\main\\resources\\testdata\\DeleteJobs.json";
			 HashMap<String, String> headers = new HashMap<String, String>();
			File file;
			
			headers.put("Content-Type", "application/json");
			file = new File(path);
			Response response = postWithResponse(REST_URL_PURCHASE_JOBS,file,null,headers);
			System.out.println("message body in deleteJobs() " + response.getBody().asString());
			System.out.println("status code " + response.getStatusCode());
			verifyContains(response.getBody().asString(), "\"data\":true", "failed to delete exiting purchased jobs",test);
			verifyEquals(response.getStatusCode(), 200, test);

		} catch (Error e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			reports.flush();
		
		}
		
		
	
	}

	

	
}
