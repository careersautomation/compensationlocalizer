package ui.seleniumgrid.testcases;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;


import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.DashBoardPage;
import pages.GoogleHomePage;
import pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

public class TestGoogleSearchInIE extends InitTests {
	DashBoardPage home;


	@Test( enabled = true)
	public void testSearchInIE() throws Exception {
		 WebDriver driver=null;
		  ExtentTest test=null;
		  Driver driverObj=new Driver();
		try {
			test = reports.createTest("testSearchInIE");
			test.assignCategory("smoke");
			driver=driverObj.initWebDriver( "https://www.google.co.in","IE","","windows","grid",test,"");
			GoogleHomePage ghomeIe=new GoogleHomePage(driver);
			ghomeIe.search("selenium");
			waitForElementToDisplay(GoogleHomePage.firstLnk);
			verifyElementTextContains(GoogleHomePage.firstLnk, "selenium",test);
			

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

			

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();


		} 
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	

	
	
	
@AfterSuite
public void kill()
{
	killBrowserExe("IE");


}
}
