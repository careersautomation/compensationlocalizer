package ui.regression.testcases;


import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.IOException;

import static verify.SoftAssertions.fail;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.DashBoardPage;
import pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TestLoginInMultiBrowsers extends InitTests {

/*
 * To test login with data driven from excel in different browsers and in different platforms
 */
  @Test(priority = 1, enabled = true)
 @Parameters({"browser","browserVersion","platform","execution_env","username","password"})
  public void testLogin(String browser,String browserVersion,String platform,String execution_env,String uname,String pwd) throws IOException {
	  WebDriver driver=null;
	  ExtentTest test=null;
	  Driver driverObj=new Driver();
	  try
	  {
		  test = reports.createTest("testLogin--Browser--"+browser);
			test.assignCategory("Regression");
			  driver=driverObj.initWebDriver("",browser,browserVersion,platform,execution_env,test,"");

		  LoginPage loginpage=new LoginPage(driver);
		  loginpage.login(uname, pwd);
		  DashBoardPage homepage = new DashBoardPage(driver);
			waitForElementToDisplay(DashBoardPage.countryDropdwnLnk);
			verifyElementTextContains(DashBoardPage.countryDropdwnLnk, "Choose Country",test);

	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("testLogin()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		softAssert.assertAll();


	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("testLogin()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		softAssert.assertAll();


	} 
	finally
	{
		reports.flush();
		driver.close();

	}
  }
}
