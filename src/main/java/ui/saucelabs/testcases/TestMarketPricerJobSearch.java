package ui.saucelabs.testcases;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;


import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import listeners.RetryListener;
import pages.DashBoardPage;
import pages.LoginPage;
import utilities.InitTests;
import utilities.RetryAnalyzer;
import verify.SoftAssertions;

import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

public class TestMarketPricerJobSearch extends InitTests {
	DashBoardPage home;

	@Test(priority = 1, enabled = true)
	public void testSearchJob() throws Exception {
		  WebDriver driver=null;
		  ExtentTest test=null;
		  Driver driverObj=new Driver();
		try {
			test = reports.createTest("testSearchJob");
			test.assignCategory("smoke");
			driver=driverObj.initWebDriver( BASEURL,"safari","latest","Mac 10.13","saucelabs",test,"");
			LoginPage loginPage = new LoginPage(driver);
			loginPage.login(USERNAME,PASSWORD);
			home = new DashBoardPage(driver);
			home.searchJob("Greece");
			waitForElementToDisplay(DashBoardPage.jobCode);
			verifyElementTextContains(DashBoardPage.jobCode, "FIN.03.046.P30",test);
			verifyElementTextContains(DashBoardPage.jobDesc, "Corporate Financial Planning",test);
			verifyElementTextContains(DashBoardPage.jobTitle,
					"Corporate Financial Planning & Analysis - Senior Professional (P3)",test);

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearchJob()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearchJob()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
			driver.close();

		}
	}
	

	
@AfterSuite
public void kill()
{
	killBrowserExe("chrome");

}
}
