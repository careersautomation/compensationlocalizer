package pages.CompensationLocalizer_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;


public class Input {
	public static int AnnualBasePayValue;
	public static int Allowance1=0;
	public static int Allowance2=0;
	public static int Deduction1=0;
	public static int Deduction2=0;
	public static int AdditionalTaxableIncomeValue=0;
	public static int ProposedHostBasePayValue=0;
	public static int HomeTotalCompensationValue=0;
	public static int HostTotalCompensationValue=0;
	public static int ProposedHostTotalCompensationValue=0;
	public static int HomeNetIncomeValue=0;
	public static int HostNetIncomeValue=0;
	public static int ProposedHostNetIncomeValue=0;
	public static int HomeAdjustedNetValue=0;
	public static int HostAdjustedNetValue=0;
	public static int ProposedHostAdjustedNetValue=0;
	public static int HostCalculatedBasePayValue=0;
	public static int HostSocialSecurityValue=0;
	public static int HostFamilyAllowanceValue=0;
	public static int HostPersonalIncomeTaxValue=0;
	public static int HostHousingValue=0;
	public static int ProposedHostSocialSecurityValue=0;
	public static int ProposedHostFamilyAllowanceValue=0;
	public static int ProposedHostPersonalIncomeTaxValue=0;
	public static int ProposedHostHousingValue=0;
	public static int SocialSecurityValue=0;;
	public static int PersonalIncomeTaxValue=0;
	public static int FamilyAllowanceValue=0;
	public static int NetIncomeValue=0;
	public static int HousingValue=0;
	public static int SpendableIncomeValue=0;
	public static int COLIValue=0;
	public static int COLAValue=0;
	public static int AdjustedNetIncomeValue=0;
	public static double ExchangeRateValue=0;
	public static int AdjustedNetIncomeInHostValue=0;
	public static int EducationCostValue=0;
	public static int HardshipPremiumValue=0;
	public static int CustomAllowanceValue=0;
	@FindBy(xpath = "//label[text()='Home Location']//following::label[1]")
	public WebElement SelectedHomeLocation;
	@FindBy(xpath = "//label[text()='Host Location']//following::label[1]")
	public WebElement SelectedHostLocation;
	@FindBy(id = "salary")
	WebElement AnnualBasePay;
	@FindBy(id = "Id_LableAdditionalTaxableFirst")
	WebElement TaxableCompensation1;
	@FindBy(id = "Id_LableAdditionalTaxableSecond")
	WebElement TaxableCompensation2;
	@FindBy(id = "Id_LableTaxDeductibleFirst")
	WebElement TaxableDeduction1;
	@FindBy(id = "Id_LableTaxDeductibleSecond")
	WebElement TaxableDeduction2;
	@FindBy(id = "ATI1")
	WebElement Compensation1_Options;//Fixed Amount
	@FindBy(id = "ATI2")
	WebElement Compensation2_Options;//% of Gross Base Pay
	@FindBy(id = "TDI1")
	WebElement Deduction1_Options;//Fixed Amount
	@FindBy(id = "TDI2")
	WebElement Deduction2_Options;//% of Gross Base Pay
	@FindBy(id = "inputId_ATCFirst")
	WebElement InputCompensation1;
	@FindBy(id = "inputId_ATCSecond")
	WebElement InputCompensation2;
	@FindBy(id = "inputId_TDCFirst")
	WebElement InputDeduction1;
	@FindBy(id = "inputId_TDCSecond")
	WebElement InputDeduction2;
	@FindBy(xpath = "//button[@title='Add another' and contains(@data-bind,'ATC')]")
	WebElement AddAnotherCompensation;
	@FindBy(xpath = "//button[@title='Add another' and contains(@data-bind,'TDC')]")
	WebElement AddAnotherDeduction;
	@FindBy(xpath= "//select[contains(@data-bind,'SelectedFamilyStatusHome')]")
	WebElement MaritalStatusHome;//Married with 3 children
	@FindBy(xpath= "//select[contains(@data-bind,'SelectedFamilyStatusHost')]")
	WebElement MaritalStatusHost;//Married with no children
	@FindBy(id = "ExchangeRateList")
	WebElement ExchangeRateList;//Specific Month
	@FindBy(id = "IdTargetSalaryInHost")
	WebElement IsHostTargetSalary;//Yes
	@FindBy(id = "ddlUseIPE")
	WebElement IsMercerIPESalary;
	@FindBy(id = "HomeCostCategory")
	WebElement HomeAreaCategory;
	@FindBy(id = "CostCategory")
	WebElement HostAreaCategory;
	@FindBy(xpath= "//select[contains(@data-bind,'.HomeHousingData')]")
	WebElement HomeHousingData;
	@FindBy(xpath= "//select[contains(@data-bind,'.Housing')]")
	WebElement HostHousingData;
	@FindBy(id = "PropHostLocationIncome")
	WebElement ProposedHostLocationIncome;
	@FindBy(xpath = "//select[contains(@data-bind,'selectionChangedUseStandardAssumptions')]")
	WebElement IsMercerStandardAssumptions;//No
	@FindBy(xpath = "//select[contains(@data-bind,'IsAdditionalTaxableIncome')]")
	WebElement IsAdditionaltaxableIncome;//Yes
	@FindBy(id = "ddlATI")
	WebElement AdditionalTaxableIncomeOptions;//Fixed Amount 
	@FindBy(id = "inputId_ATI")
	WebElement InputAdditionalTaxableIncome;
	@FindBy(id = "inputCheckbox")
	WebElement AcknowledgementCheckBox;
	@FindBy(id = "btnContinue")
	WebElement ContinueButton;
	@FindBy(xpath = "//img[@class='loadingImage']")
	WebElement BufferImage;

	public Input()
	{
		PageFactory.initElements(driver, this);
	}
	public void initialise() {
		Allowance1=0;
		Allowance2=0;
		Deduction1=0;
		Deduction2=0;
		AdditionalTaxableIncomeValue=0;
		ProposedHostBasePayValue=0;
		SocialSecurityValue=0;;
		PersonalIncomeTaxValue=0;
		HomeTotalCompensationValue=0;
		HostTotalCompensationValue=0;
		 ProposedHostTotalCompensationValue=0;
		 HomeNetIncomeValue=0;
		 HostNetIncomeValue=0;
		 ProposedHostNetIncomeValue=0;
		 HomeAdjustedNetValue=0;
		 HostAdjustedNetValue=0;
		 ProposedHostAdjustedNetValue=0;
		 HostCalculatedBasePayValue=0;
		 HostSocialSecurityValue=0;
		 HostFamilyAllowanceValue=0;
		 HostPersonalIncomeTaxValue=0;
		 HostHousingValue=0;
		 ProposedHostSocialSecurityValue=0;
		 ProposedHostFamilyAllowanceValue=0;
		 ProposedHostPersonalIncomeTaxValue=0;
		 ProposedHostHousingValue=0;
		 SocialSecurityValue=0;;
		 PersonalIncomeTaxValue=0;
		 FamilyAllowanceValue=0;
		 NetIncomeValue=0;
		 HousingValue=0;
		 SpendableIncomeValue=0;
		 COLIValue=0;
		 COLAValue=0;
		 AdjustedNetIncomeValue=0;
		ExchangeRateValue=0;
		 AdjustedNetIncomeInHostValue=0;
		 EducationCostValue=0;
		 HardshipPremiumValue=0;
		 CustomAllowanceValue=0;
	}
	public void setBaseSalary(String salary) {
		setInput(AnnualBasePay,salary);
		AnnualBasePayValue = Integer.parseInt(salary);		
	}
	public int setGrossPercentOrAmount(WebElement dropdown,WebElement inputBox, String value) {
		if(value.charAt(1)=='%'|| value.charAt(2)=='%'){
			selEleByVisbleText(dropdown,"% of Gross Base Pay");
			int length=(value.length())-1;
			value=value.substring(0,length);
			setInput(inputBox,value);
			System.out.println(value);
			int amount= (AnnualBasePayValue*Integer.parseInt(value))/100;
			System.out.println(amount);
			return amount;			
		}
		else {
			selEleByVisbleText(dropdown,"Fixed Amount");
			setInput(inputBox,value);
			return Integer.parseInt(value);
		}
	}
	public void setAllowances(String allowance1, String allowance2) {
		setInput(TaxableCompensation1,"Allowance1");
		Allowance1 = setGrossPercentOrAmount(Compensation1_Options,InputCompensation1,allowance1);
		clickElement(AddAnotherCompensation);
		setInput(TaxableCompensation2,"Allowance2");
		Allowance2 = setGrossPercentOrAmount(Compensation2_Options,InputCompensation2,allowance2);
	}
	
	public void setDeductions(String deduction1, String deduction2) {
		setInput(TaxableDeduction1,"Deduction1");
		Deduction1 = setGrossPercentOrAmount(Deduction1_Options,InputDeduction1,deduction1);
		clickElement(AddAnotherDeduction);
		setInput(TaxableDeduction2,"Deduction2");
		Deduction2 = setGrossPercentOrAmount(Deduction2_Options,InputDeduction2,deduction2);
	}
	
	public void setMaritalStatus(String maritalHome,String maritalHost) {
		selEleByVisbleText(MaritalStatusHome,maritalHome);
		selEleByVisbleText(MaritalStatusHost,maritalHost);
		
	}
	public void setExchangeRate(String option) {
		selEleByVisbleText(ExchangeRateList,option);
		
	}
	public void setTargetSalary(String choice,String income) {
		if(choice=="Yes") {
		selEleByVisbleText(IsHostTargetSalary,choice);
		setInput(ProposedHostLocationIncome,income);
		ProposedHostBasePayValue= Integer.parseInt(income);
		}
		else {
			selEleByVisbleText(IsHostTargetSalary,choice);
			selEleByVisbleText(IsMercerIPESalary,"Yes");
		}
	}
	public void setMercerStandardAssumptions(String value) {
		selEleByVisbleText(IsMercerStandardAssumptions,value);
	}
	public void setHousingData(String homehousing,String homeArea,String hosthousing,String hostArea) {
		selEleByVisbleText(HomeHousingData,homehousing);
		selEleByVisbleText(HomeAreaCategory,homeArea);
		selEleByVisbleText(HostHousingData,hosthousing);
		selEleByVisbleText(HostAreaCategory,hostArea);
		
	}
	public void setAdditionalTaxableIncome(String income) {
		selEleByVisbleText(IsAdditionaltaxableIncome,"Yes");
		AdditionalTaxableIncomeValue = setGrossPercentOrAmount(AdditionalTaxableIncomeOptions,InputAdditionalTaxableIncome,income);
		
	}
	public void submit() {
		clickElement(AcknowledgementCheckBox);
		clickElement(ContinueButton);
		delay(10000);
	}
}
